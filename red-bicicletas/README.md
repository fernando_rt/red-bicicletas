# Red de Bicicletas
Proyecto desarrollado en Node con Express. 
Muestra un mapa con marcadores en la ciudad de Querétaro, México.
Se implementó un modelo de usuarios, el cual sus operaciones CRUD están disponibles en `/usuarios`
Se tiene la funcionalidad en el navegador y por medio de API para altas, modificaciones y eliminaciones de bicicletas. Estas funcionalidades con seguridad por medio login y reseteo de contraseña para el navegador y uso de JWT para el manejo por API.
Se tiene funcionalidad de login por medio de Google y Facebook (este último funcionando sólo por API).
Funcionalidad de monitoreo de aplicación por medio de newrelic.


## Para correr el proyecto
### git clone https://fernando_rt@bitbucket.org/fernando_rt/red-bicicletas.git
Clonar el repositorio

### npm install
Instalación de dependencias.

### npm run start
Para correr el servidor en modo productivo

### npm run devstart
Inicia un servidor con nodemon para modo de desarrollo.

### npm run test
Para correr todas las pruebas unitarias.