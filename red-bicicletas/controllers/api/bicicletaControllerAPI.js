var Bicicleta = require("../../models/bicicleta");

exports.bicicleta_list = function (req, res) {
  Bicicleta.find({}, function (err, bicicletas) {
    res.status(200).json({ bicicletas });
  });
};

exports.bicicleta_create = function (req, res) {
  const { id: code, color, modelo, lat, lng } = req.body;
  const bicicleta = new Bicicleta({
    code,
    color,
    modelo,
    ubicacion: [lng, lat],
  });
  Bicicleta.add(bicicleta, (err) => {
    res.status(200).json(bicicleta);
  });
};

exports.bicicleta_delete = function (req, res) {
  const { id: code } = req.body;
  Bicicleta.deleteByCode(code, (err) => {
    res.status(204).send();
  });
};

exports.bicicleta_update = function (req, res) {
  const { id: code, color, modelo, lat, lng } = req.body;
  Bicicleta.findOneAndUpdate(
    { code: req.params.id },
    { code, color, modelo, ubicacion: [lng, lat] },
    { new: true },
    (err, bicicleta) => {
      res.status(200).json(bicicleta);
    }
  );
};
