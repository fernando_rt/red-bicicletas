var Usuario = require("../../models/usuario");

exports.usuarios_list = function (req, res) {
  Usuario.find({}, function (err, usuarios) {
    res.status(200).json({ usuarios });
  });
};

exports.usuarios_create = function (req, res) {
  const { nombre, email, password } = req.body;
  const usuario = new Usuario({ nombre, email, password });
  usuario.save(function (err) {
    if (err) return res.status(500).json(err);
    res.status(200).json(usuario);
  });
};

exports.usuario_reservar = function (req, res) {
  const { id, bici_id, desde, hasta } = req.body;
  Usuario.findById(id, function (err, usuario) {
    usuario.reservar(bici_id, desde, hasta, function (err) {
      res.status(200).send();
    });
  });
};
