var Bicicleta = require("../models/bicicleta");

exports.bicicleta_list = function (req, res) {
  Bicicleta.find({}, (err, bicis) => {
    res.render("bicicletas/index", { bicis });
  });
};

exports.bicicleta_create_get = function (req, res) {
  res.render("bicicletas/create", { errors: {}, bici: new Bicicleta() });
};

exports.bicicleta_create_post = function (req, res) {
  const { code, color, modelo, lat, lng } = req.body;
  Bicicleta.create({ code, color, modelo, ubicacion: [lat, lng] }, function (
    err
  ) {
    if (err) {
      res.render("bicicletas/create", { errors: err.errors });
    } else {
      res.redirect("/bicicletas");
    }
  });
};

exports.bicicleta_update_get = function (req, res) {
  Bicicleta.findById(req.params.id, function (err, bici) {
    res.render("bicicletas/update", { bici });
  });
};

exports.bicicleta_update_post = function (req, res) {
  const { code, color, modelo, lat, lng } = req.body;
  const update_values = { code, color, modelo, ubicacion: [lat, lng] };
  Bicicleta.findByIdAndUpdate(req.params.id, update_values, function (
    err,
    bici
  ) {
    if (err)
      res.render("bicicletas/update", {
        errors: err.errors,
        bici: new Bicicleta({ code, color, modelo, ubicacion: [lat, lng] }),
      });
    else res.redirect("/bicicletas");
  });
};

exports.bicicleta_delete_post = function (req, res, next) {
  Bicicleta.findByIdAndDelete(req.body.id, function (err) {
    if (err) {
      console.log(err);
      next(err);
    } else res.redirect("/bicicletas");
  });
};
