var mongoose = require("mongoose");
var Bicicleta = require("../../models/bicicleta");
var server = require("../../bin/www");
var request = require("request");
var base_url = "http://localhost:5000/api/bicicletas";

describe("Bicicleta API", () => {
  beforeEach((done) => {
    var mongoDB = "mongodb://localhost/testdb";
    mongoose.connect(mongoDB, { useNewUrlParser: true });

    const db = mongoose.connection;
    db.on("error", console.error.bind(console, "connection error"));
    db.once("open", function () {
      console.log("Connected to testDB");
      done();
    });
  });

  afterEach((done) => {
    Bicicleta.deleteMany({}, function (err, success) {
      if (err) console.log(err);
      done();
    });
  });

  describe("GET Bicicletas /", () => {
    it("Status 200", (done) => {
      request.get(base_url, (err, res, body) => {
        console.log(body);
        var result = JSON.parse(res.body);
        expect(res.statusCode).toBe(200);
        expect(result.bicicletas.length).toBe(0);
        done();
      });
    });
  });

  describe("POST Bicicletas /create", () => {
    it("Status 200", (done) => {
      const headers = { "content-type": "application/json" };
      const body =
        '{ "id": 2, "color": "rojo", "modelo": "urbana", "lat": 15, "lng": 15 }';
      request.post(
        {
          headers,
          body,
          url: `${base_url}/create`,
        },
        (err, res, body) => {
          const bici = JSON.parse(body);
          expect(res.statusCode).toBe(200);
          console.log(bici);
          expect(bici.color).toBe("rojo");
          expect(bici.modelo).toBe("urbana");
          expect(bici.ubicacion[0]).toBe(15);
          expect(bici.ubicacion[1]).toBe(15);
          done();
        }
      );
    });
  });

  describe("DELETE Bicicletas /delete", () => {
    it("Status 204", (done) => {
      const headers = { "content-type": "application/json" };
      const body = '{ "id": 1 }';
      request.delete(
        {
          headers,
          body,
          url: `${base_url}/delete`,
        },
        (err, res, body) => {
          expect(res.statusCode).toBe(204);
          done();
        }
      );
    });
  });

  describe("POST Bicicletas /update", () => {
    it("Status 200", (done) => {
      const nueva = new Bicicleta({
        code: 1,
        color: "azul",
        modelo: "urbana",
        ubicacion: [10, 10],
      });
      Bicicleta.add(nueva, (err, newBici) => {
        console.log(newBici);
      });

      const headers = { "content-type": "application/json" };
      const body =
        '{ "id": 1, "color": "verde", "modelo": "montaña", "lat": 16, "lng": 16 }';
      request.post(
        {
          headers,
          body,
          url: `${base_url}/1/update`,
        },
        (err, res, body) => {
          const bicicleta = JSON.parse(body);
          expect(res.statusCode).toBe(200);
          expect(bicicleta.modelo).toBe("montaña");
          expect(bicicleta.color).toBe("verde");
          expect(bicicleta.ubicacion).toEqual([16, 16]);
          done();
        }
      );
    });
  });
});
