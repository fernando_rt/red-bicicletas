var mongoose = require("mongoose");
var Reserva = require("../../models/reserva");
var Bicicleta = require("../../models/bicicleta");
var Usuario = require("../../models/usuario");

describe("Testing usuarios", () => {
  beforeEach((done) => {
    const mongodb = "mongodb://localhost/testdb";
    mongoose.connect(mongodb, { useNewUrlParser: true });
    const db = mongoose.connection;
    db.on("error", console.error.bind(console, "connection error"));
    db.once("open", () => {
      console.log("Connected to DB");
      done();
    });
  });

  afterEach((done) => {
    Reserva.deleteMany({}, (err, success) => {
      if (err) console.log(err);
      Usuario.deleteMany({}, (err, success) => {
        if (err) console.log(err);
        Bicicleta.deleteMany({}, (err, success) => {
          if (err) console.log(err);
          done();
        });
      });
    });
  });

  describe("Cuando un usuario reserva una bici", () => {
    it("debe generar una reserva", (done) => {
      const usuario = new Usuario({
        nombre: "Fernando",
        email: "fernando@mail.com",
        password: "123",
      });
      usuario.save();
      const bici = new Bicicleta({ code: 1, color: "rojo", modelo: "urbana" });
      bici.save();

      var hoy = new Date();
      var manana = new Date();
      manana.setDate(hoy.getDate() + 1);

      usuario.reservar(bici.id, hoy, manana, function (err, reserva) {
        Reserva.find({})
          .populate("bicicleta")
          .populate("usuario")
          .exec(function (err, reservas) {
            const reserva = reservas[0];
            console.log(reserva);
            expect(reservas.length).toBe(1);
            expect(reserva.diasDeReserva()).toBe(2);
            expect(reserva.bicicleta.code).toBe(1);
            expect(reserva.usuario.nombre).toBe(usuario.nombre);
            done();
          });
      });
    });
  });
});
