var mongoose = require("mongoose");
var Bicicleta = require("../../models/bicicleta");

describe("Testing bicicletas", () => {
  beforeEach((done) => {
    const mongodb = "mongodb://localhost/testdb";
    mongoose.connect(mongodb, { useNewUrlParser: true });
    const db = mongoose.connection;
    db.on("error", console.error.bind(console, "connection error"));
    db.once("open", () => {
      console.log("Connected to DB");
      done();
    });
  });

  afterEach((done) => {
    Bicicleta.deleteMany({}, (err, success) => {
      if (err) console.log(err);
      done();
    });
  });

  describe("Bicicleta.createInstance", () => {
    it("Crea instancia de Bicicleta", () => {
      const bici = new Bicicleta({
        code: 1,
        color: "verde",
        modelo: "urbana",
        ubicacion: [0, 0],
      });
      expect(bici.code).toBe(1);
    });
  });

  describe("Bicicleta.allBicis", () => {
    it("Debe iniciar vacía", (done) => {
      Bicicleta.allBicis((err, bicis) => {
        expect(bicis.length).toBe(0);
        done();
      });
    });
  });

  describe("Bicicleta.add", () => {
    it("Debe agregar una bicicleta", (done) => {
      const aBici = new Bicicleta({
        code: 1,
        color: "verde",
        modelo: "urbana",
        ubicacion: [0, 0],
      });
      Bicicleta.add(aBici, (err, newBici) => {
        if (err) console.log(err);
        Bicicleta.allBicis((err, bicis) => {
          expect(bicis.length).toBe(1);
          expect(bicis[0].code).toEqual(aBici.code);
          done();
        });
      });
    });
  });

  describe("Bicicleta.findByCode", () => {
    it("Debe devolver la bici con code 1", (done) => {
      Bicicleta.allBicis((err, bicis) => {
        expect(bicis.length).toBe(0);

        const aBici = new Bicicleta({
          code: 1,
          color: "verde",
          modelo: "urbana",
        });
        Bicicleta.add(aBici, (err, newBici) => {
          if (err) console.log(err);
          const aBici2 = new Bicicleta({
            code: 2,
            color: "rojo",
            modelo: "montaña",
          });
          Bicicleta.add(aBici2, (err, newBici) => {
            if (err) console.log(err);
            Bicicleta.findByCode(1, (err, target) => {
              expect(target.code).toBe(aBici.code);
              expect(target.color).toBe(aBici.color);
              expect(target.modelo).toBe(aBici.modelo);
              done();
            });
          });
        });
      });
    });
  });

  describe("Bicicleta.deleteByCode", () => {
    it("Debe remover la bici con code 1", (done) => {
      Bicicleta.allBicis((err, bicis) => {
        if (err) console.log(err);
        expect(bicis.length).toBe(0);

        const aBici = new Bicicleta({
          code: 1,
          color: "verde",
          modelo: "urbana",
        });
        Bicicleta.add(aBici, (err) => {
          if (err) console.log(err);
          const aBici2 = new Bicicleta({
            code: 2,
            color: "rojo",
            modelo: "montaña",
          });
          Bicicleta.add(aBici2, (err) => {
            if (err) console.log(err);
            Bicicleta.deleteByCode(1, (err) => {
              if (err) console.log(err);
              Bicicleta.allBicis((err, bicis) => {
                if (err) console.log(err);
                expect(bicis.length).toBe(1);
                done();
              });
            });
          });
        });
      });
    });
  });
});
