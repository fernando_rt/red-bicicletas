var mongoose = require("mongoose");
const bcrypt = require("bcrypt");
const crypto = require("crypto");
const validator = require("mongoose-unique-validator");
const mailer = require("../mailer/mailer");
var Reserva = require("./reserva");
var Token = require("./token");

const saltRounds = 10;

var Schema = mongoose.Schema;

const validateEmail = (email) => {
  const re = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
  return re.test(email);
};

var usuarioSchema = new Schema({
  nombre: {
    type: String,
    trim: true,
    required: [true, "El nombre es obligatorio"],
  },
  email: {
    type: String,
    trim: true,
    required: [true, "El correo es obligatorio"],
    lowercase: true,
    validate: [validateEmail, "El correo es inválido"],
    match: [/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/],
    unique: true,
  },
  password: {
    type: String,
    trim: true,
    required: [true, "La contraseña es obligatoria"],
  },
  passwordResetToken: String,
  passwordResetTokenExpires: Date,
  verificado: { type: Boolean, default: false },
  googleId: String,
  facebookId: String,
});

usuarioSchema.plugin(validator, { message: "El usuario {PATH} ya existe" });

usuarioSchema.pre("save", function (next) {
  if (this.isModified("password")) {
    this.password = bcrypt.hashSync(this.password, saltRounds);
  }
  next();
});

usuarioSchema.methods.validPassword = function (password) {
  return bcrypt.compareSync(password, this.password);
};

usuarioSchema.methods.reservar = function (bicicleta, desde, hasta, cb) {
  var reserva = new Reserva({ usuario: this._id, bicicleta, desde, hasta });
  reserva.save(cb);
};

usuarioSchema.methods.enviar_email_bienvenida = function (cb) {
  const token = new Token({
    _userId: this.id,
    token: crypto.randomBytes(16).toString("hex"),
  });
  const email_destination = this.email;
  token.save(function (err) {
    if (err) return console.log(err.message);

    const mailOptions = {
      from: "A00987152@itesm.mx",
      to: email_destination,
      subject: "Verificación de cuenta",
      text: `Verifica tu cuenta haciendo clic en el siguiente link \n
      ${
        process.env.HOST === "localhost"
          ? "http://localhost:5000"
          : process.env.HOST
      }/token/confirmation/${token.token}`,
    };

    mailer.sendMail(mailOptions, function (err) {
      if (err) return console.log(err.message);
      console.log(`Correo de verificación enviado a: ${email_destination}`);
    });
  });
};

usuarioSchema.methods.resetPassword = function (cb) {
  const token = new Token({
    _userId: this.id,
    token: crypto.randomBytes(16).toString("hex"),
  });
  const email_destination = this.email;
  token.save(function (err) {
    if (err) return cb(err);

    const mailOptions = {
      from: "A00987152@itesm.mx",
      to: email_destination,
      subject: "Reseteo de contraseña",
      text: `Para resetear su constraseña haz clic en el siguiente link \n
      ${
        process.env.HOST === "localhost"
          ? "http://localhost:5000"
          : process.env.HOST
      }/token/confirmation/${token.token}`,
    };

    mailer.sendMail(mailOptions, function (err) {
      if (err) return cb(err);
      console.log(
        `Correo de reseteo de contraseña enviado a: ${email_destination}`
      );
    });
    cb(null);
  });
};

usuarioSchema.statics.findOneOrCreateByGoogle = function findOrCreate(
  condition,
  callback
) {
  const self = this;
  console.log(condition);
  self.findOne(
    {
      $or: [
        {
          googleId: condition.id,
          email: condition.emails[0].value,
        },
      ],
    },
    (err, result) => {
      if (result) {
        callback(err, result);
      } else {
        console.log("=====CONDITION=====");
        console.log(condition);
        let values = {};
        values.googleId = condition.id;
        values.email = condition.emails[0].value;
        values.nombre = condition.displayName || "Sin Nombre";
        values.verificado = true;
        values.password = condition._json.etag;
        console.log("=====VALUES=====");
        console.log(values);
        self.create(values, (err, result) => {
          if (err) console.log(err);
          return callback(err, result);
        });
      }
    }
  );
};

usuarioSchema.statics.findOneOrCreateByFacebook = function findOrCreate(
  condition,
  callback
) {
  const self = this;
  console.log(condition);
  self.findOne(
    {
      $or: [
        {
          facebookId: condition.id,
          email: condition.emails[0].value,
        },
      ],
    },
    (err, result) => {
      if (result) {
        callback(err, result);
      } else {
        console.log("=====CONDITION=====");
        console.log(condition);
        let values = {};
        values.facebookId = condition.id;
        values.email = condition.emails[0].value;
        values.nombre = condition.displayName || "Sin Nombre";
        values.verificado = true;
        values.password = crypto.randomBytes(16).toString("hex");
        console.log("=====VALUES=====");
        console.log(values);
        self.create(values, (err, result) => {
          if (err) console.log(err);
          return callback(err, result);
        });
      }
    }
  );
};

module.exports = mongoose.model("Usuario", usuarioSchema);
