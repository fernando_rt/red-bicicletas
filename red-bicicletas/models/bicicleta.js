var mongoose = require("mongoose");
var Schema = mongoose.Schema;

var bicicletaSchema = new Schema({
  code: Number,
  color: String,
  modelo: String,
  ubicacion: { type: [Number], index: { type: "2dsphere", sparse: true } },
});

bicicletaSchema.methods.toString = () =>
  `code: ${this.code} color:${this.color} modelo: ${this.modelo}`;

bicicletaSchema.statics.allBicis = function (cb) {
  return this.find({}, cb);
};

bicicletaSchema.statics.createInstance = (code, color, modelo, ubicacion) =>
  new this({
    code,
    color,
    modelo,
    ubicacion,
  });

bicicletaSchema.statics.add = function (bici, cb) {
  return this.create(bici, cb);
};

bicicletaSchema.statics.findByCode = function (code, cb) {
  return this.findOne({ code }, cb);
};

bicicletaSchema.statics.deleteByCode = function (code, cb) {
  return this.deleteOne({ code }, cb);
};

module.exports = mongoose.model("Bicicleta", bicicletaSchema);